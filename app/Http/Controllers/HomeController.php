<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    public function retrieveID(Request $request)
    {   
        $prime_numbers = $this->getPrimeNumbers();
        dd(substr($prime_numbers, $request->num, 5));
    }

    public function getPrimeNumbers()
    {
        $prime_numbers='';
        for($i=2; $i<10000;$i++)
        {
            for($x=2;$x<$i;$x++)
            {
                if($i%$x==0)
                {
                    continue 2;
                }
            }

            $prime_numbers .= $i;
        }
        
        return $prime_numbers;
    }
}
